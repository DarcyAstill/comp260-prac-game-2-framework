﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class MovePaddle : MonoBehaviour
{
    private new Rigidbody rigidbody;
    public float speed;
    // Use this for initialization
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.useGravity = false;
    }


    void FixedUpdate()
    {
        Vector3 pos = GetMousePosition();
        Vector3 dir = pos - rigidbody.position;
        Vector3 vel = dir.normalized * speed;


        float move = speed * Time.fixedDeltaTime;
        float distToTarget = dir.magnitude;

        if (move > distToTarget)
        {
            vel = vel * distToTarget / move;
        }

        rigidbody.velocity = vel;

    }
    private Vector3 GetMousePosition()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        Plane plane = new Plane(Vector3.up, Vector3.zero);
        float distance = 0;
        plane.Raycast(ray, out distance);
        return ray.GetPoint(distance);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Vector3 pos = GetMousePosition();
        Gizmos.DrawLine(Camera.main.transform.position, pos);
    }
}

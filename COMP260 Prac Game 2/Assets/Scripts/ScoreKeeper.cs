﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreKeeper : MonoBehaviour
{

    static private ScoreKeeper instance;
    public int pointsPerGoal = 1;
    private int[] score = new int[2];
    public Text[] scoreText;
    public Text PlayerWins;
    public GameObject Puck;

    void Start()
    {
        if (instance == null)
        {
            // save this instance
            instance = this;
        }
        else
        {
            // more than one instance exists
            Debug.LogError(
                "More than one Scorekeeper exists in the scene.");
        }
        for (int i = 0; i < score.Length; i++)
        {
            score[i] = 0;
            scoreText[i].text = "0";
        }

    }
    static public ScoreKeeper Instance
    {
        get { return instance; }
    }

    public void ScoreGoal(int player)
    {
        if (score[player] < 2)
        {
            score[player] += pointsPerGoal;
            scoreText[player].text = score[player].ToString();
        }
        else
        {

            score[player] += pointsPerGoal;
            scoreText[player].text = score[player].ToString();
            PlayerWins.text = "Player " + player.ToString() + " Wins";
            PlayerWins.gameObject.SetActive(true);
            Puck.gameObject.SetActive(false);
        }

    }



}

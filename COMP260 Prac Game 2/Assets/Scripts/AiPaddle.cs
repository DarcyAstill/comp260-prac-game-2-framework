﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
public class AiPaddle : MonoBehaviour
{

    public GameObject Puck;
    public GameObject OwnGoal;
    private new Rigidbody rigidbody;
    public float speed;


    void Start()
    {
        //Grabs the ai's rigidbody
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.useGravity = false;
    }

    void FixedUpdate()
    {
        //Defence AI 
        //Changed the pos to be between the puck and the goal 
        Vector3 pos = 0.5f * (Puck.transform.position + OwnGoal.transform.position);
        Vector3 dir = pos - rigidbody.position;
        Vector3 vel = dir.normalized * speed;

        //Movement functions as normal
        float move = speed * Time.fixedDeltaTime;
        float distToTarget = dir.magnitude;

        if (move > distToTarget)
        {
            vel = vel * distToTarget / move;
        }

        rigidbody.velocity = vel;

    }

}
